package it.uniroma2.art.vartrans.vocabolary;

import it.uniroma2.art.owlart.exceptions.VocabularyInitializationException;
import it.uniroma2.art.owlart.model.ARTNodeFactory;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.vocabulary.VocabUtilities;

public class VARTRANS {

    public static final String URI = "http://www.w3.org/ns/lemon/vartrans";
    public static final String NAMESPACE = "http://www.w3.org/ns/lemon/vartrans#";

    public static final String LEXICO_SEMANTIC_RELATION = "http://www.w3.org/ns/lemon/vartrans#LexicoSemanticRelation";
    public static final String LEXICAL_RELATION = "http://www.w3.org/ns/lemon/vartrans#LexicalRelation";
    public static final String SENSE_RELATION = "http://www.w3.org/ns/lemon/vartrans#SenseRelation";
    public static final String TERMINOLOGICAL_RELATION = "http://www.w3.org/ns/lemon/vartrans#TerminologicalRelation";
    public static final String TRANSLATION_CLASS = "http://www.w3.org/ns/lemon/vartrans#Translation";
    public static final String TRANSLATION_SET = "http://www.w3.org/ns/lemon/vartrans#TranslationSet";

    public final static String LEXICAL_REL = "http://www.w3.org/ns/lemon/vartrans#lexicalRel";
    public final static String SENSE_REL = "http://www.w3.org/ns/lemon/vartrans#senseRel";
    public static final String RELATES = "http://www.w3.org/ns/lemon/vartrans#relates";
    public static final String SOURCE = "http://www.w3.org/ns/lemon/vartrans#source";
    public static final String TARGET = "http://www.w3.org/ns/lemon/vartrans#target";
    public static final String CATEGORY = "http://www.w3.org/ns/lemon/vartrans#category";
    public static final String TRANSLATION_PROPERTY = "http://www.w3.org/ns/lemon/vartrans#translation";
    public static final String TRANSLATABLE_AS = "http://www.w3.org/ns/lemon/vartrans#translatableAs";
    public static final String TRANS = "http://www.w3.org/ns/lemon/vartrans#trans";

    public static class Res {

        public static ARTURIResource URI;
        public static ARTURIResource NAMESPACE;

        public static ARTURIResource LEXICO_SEMANTIC_RELATION;
        public static ARTURIResource LEXICAL_RELATION;
        public static ARTURIResource SENSE_RELATION;
        public static ARTURIResource TERMINOLOGICAL_RELATION;
        public static ARTURIResource TRANSLATION_CLASS;
        public static ARTURIResource TRANSLATION_SET;

        public static ARTURIResource LEXICAL_REL;
        public static ARTURIResource SENSE_REL;
        public static ARTURIResource RELATES;
        public static ARTURIResource SOURCE;
        public static ARTURIResource TARGET;
        public static ARTURIResource CATEGORY;
        public static ARTURIResource TRANSLATION_PROPERTY;
        public static ARTURIResource TRANSLATABLE_AS;
        public static ARTURIResource TRANS;

        static {
            try {
                initialize(VocabUtilities.nodeFactory);
            } catch (VocabularyInitializationException e) {
                e.printStackTrace(System.err);
            }
        }

        public static void initialize(ARTNodeFactory fact) throws VocabularyInitializationException {

            URI = fact.createURIResource(VARTRANS.URI);
            NAMESPACE = fact.createURIResource(VARTRANS.NAMESPACE);

            LEXICO_SEMANTIC_RELATION = fact.createURIResource(VARTRANS.LEXICO_SEMANTIC_RELATION);
            LEXICAL_RELATION = fact.createURIResource(VARTRANS.LEXICAL_RELATION);
            SENSE_RELATION = fact.createURIResource(VARTRANS.SENSE_RELATION);
            TERMINOLOGICAL_RELATION = fact.createURIResource(VARTRANS.TERMINOLOGICAL_RELATION);
            TRANSLATION_CLASS = fact.createURIResource(VARTRANS.TRANSLATION_CLASS);
            TRANSLATION_SET = fact.createURIResource(VARTRANS.TRANSLATION_SET);

            LEXICAL_REL = fact.createURIResource(VARTRANS.LEXICAL_REL);
            SENSE_REL= fact.createURIResource(VARTRANS.SENSE_REL);
            RELATES = fact.createURIResource(VARTRANS.RELATES);
            SOURCE = fact.createURIResource(VARTRANS.SOURCE);
            TARGET = fact.createURIResource(VARTRANS.TARGET);
            CATEGORY = fact.createURIResource(VARTRANS.CATEGORY);
            TRANSLATION_PROPERTY = fact.createURIResource(VARTRANS.TRANSLATION_PROPERTY);
            TRANSLATABLE_AS = fact.createURIResource(VARTRANS.TRANSLATABLE_AS);
            TRANS = fact.createURIResource(VARTRANS.TRANS);

        }

    }

}
