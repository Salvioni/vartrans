package it.uniroma2.art.vartrans.model.impl;

import it.uniroma2.art.vartrans.model.VARTRANSModel;
import it.uniroma2.art.ontolex.model.OntoLexModel;
import it.uniroma2.art.ontolex.model.impl.OntoLexModelImpl;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.io.RDFNodeSerializer;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.impl.URIResourceIteratorFilteringNodeIterator;
import it.uniroma2.art.owlart.models.impl.URIResourceIteratorFilteringResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTNodeIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.query.QueryLanguage;
import it.uniroma2.art.owlart.query.TupleBindings;
import it.uniroma2.art.owlart.query.TupleBindingsIterator;
import it.uniroma2.art.owlart.query.TupleQuery;
import it.uniroma2.art.owlart.utilities.RDFIterators;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.RDFS;
import it.uniroma2.art.vartrans.vocabolary.VARTRANS;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class VARTRANSModelImpl extends OntoLexModelImpl implements VARTRANSModel {

    public VARTRANSModelImpl(OntoLexModel baseRep) {
        super(baseRep);
    }

    @Override
    public ARTURIResourceIterator listLexicoSemanticRelations(ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringResourceIterator(baseRep.listSubjectsOfPredObjPair(RDF.Res.TYPE, VARTRANS.Res.LEXICO_SEMANTIC_RELATION, true, graphs));

    }

    @Override
    public ARTURIResourceIterator listLexicalRelations(ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringResourceIterator(baseRep.listSubjectsOfPredObjPair(RDF.Res.TYPE, VARTRANS.Res.LEXICAL_RELATION, true, graphs));

    }

    @Override
    public ARTURIResourceIterator listLexicoSenseRelations(ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringResourceIterator(baseRep.listSubjectsOfPredObjPair(RDF.Res.TYPE, VARTRANS.Res.SENSE_RELATION, true, graphs));

    }

    @Override
    public ARTURIResourceIterator listTerminologicalRelations(ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringResourceIterator(baseRep.listSubjectsOfPredObjPair(RDF.Res.TYPE, VARTRANS.Res.TERMINOLOGICAL_RELATION, true, graphs));

    }

    @Override
    public ARTURIResourceIterator listTranslations(ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringResourceIterator(baseRep.listSubjectsOfPredObjPair(RDF.Res.TYPE, VARTRANS.Res.TRANSLATION_CLASS, true, graphs));

    }

    @Override
    public ARTURIResourceIterator listTranslationSets(ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringResourceIterator(baseRep.listSubjectsOfPredObjPair(RDF.Res.TYPE, VARTRANS.Res.TRANSLATION_SET, true, graphs));

    }

    @Override
    public boolean isLexicoSemanticRelation(ARTURIResource resource, ARTResource... graphs) throws ModelAccessException {
        return (baseRep.hasTriple(resource, RDF.Res.TYPE, VARTRANS.Res.LEXICO_SEMANTIC_RELATION, true, graphs));
    }

    @Override
    public boolean isLexicalRelation(ARTURIResource resource, ARTResource... graphs) throws ModelAccessException {
        return (baseRep.hasTriple(resource, RDF.Res.TYPE, VARTRANS.Res.LEXICAL_RELATION, true, graphs));
    }

    @Override
    public boolean isLexicoSenseRelation(ARTURIResource resource, ARTResource... graphs) throws ModelAccessException {
        return (baseRep.hasTriple(resource, RDF.Res.TYPE, VARTRANS.Res.SENSE_RELATION, true, graphs));
    }

    @Override
    public boolean isTerminologicalRelation(ARTURIResource resource, ARTResource... graphs) throws ModelAccessException {
        return (baseRep.hasTriple(resource, RDF.Res.TYPE, VARTRANS.Res.TERMINOLOGICAL_RELATION, true, graphs));
    }

    @Override
    public boolean isTranslation(ARTURIResource resource, ARTResource... graphs) throws ModelAccessException {
        return (baseRep.hasTriple(resource, RDF.Res.TYPE, VARTRANS.Res.TRANSLATION_CLASS, true, graphs));
    }

    @Override
    public boolean isTranslationSet(ARTURIResource resource, ARTResource... graphs) throws ModelAccessException {
        return (baseRep.hasTriple(resource, RDF.Res.TYPE, VARTRANS.Res.TRANSLATION_SET, true, graphs));
    }

    @Override
    public ARTURIResource getSource(ARTURIResource lexicoSematicRelation, ARTResource... graphs) throws ModelAccessException {

        try (ARTNodeIterator it = baseRep.listValuesOfSubjPredPair(lexicoSematicRelation, VARTRANS.Res.SOURCE, false, graphs)) {

            if (it.hasNext()) {
                ARTNode next = it.next();

                if (next.isURIResource()) {
                    return next.asURIResource();
                }
            }
        }

        return null;

    }

    @Override
    public void setSource(ARTURIResource lexicoSematicRelation, ARTURIResource source, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(lexicoSematicRelation, VARTRANS.Res.SOURCE, NodeFilters.ANY, graphs);
        baseRep.addTriple(lexicoSematicRelation, VARTRANS.Res.SOURCE, source, graphs);

    }

    @Override
    public ARTURIResource getTarget(ARTURIResource lexicoSematicRelation, ARTResource... graphs) throws ModelAccessException {

        try (ARTNodeIterator it = baseRep.listValuesOfSubjPredPair(lexicoSematicRelation, VARTRANS.Res.TARGET, false, graphs)) {

            if (it.hasNext()) {
                ARTNode next = it.next();

                if (next.isURIResource()) {
                    return next.asURIResource();
                }
            }
        }

        return null;

    }

    @Override
    public void setTarget(ARTURIResource lexicoSematicRelation, ARTURIResource target, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(lexicoSematicRelation, VARTRANS.Res.TARGET, NodeFilters.ANY, graphs);
        baseRep.addTriple(lexicoSematicRelation, VARTRANS.Res.TARGET, target, graphs);

    }

    @Override
    public ARTURIResource getCategory(ARTURIResource lexicoSematicRelation, ARTResource... graphs) throws ModelAccessException {

        try (ARTNodeIterator it = baseRep.listValuesOfSubjPredPair(lexicoSematicRelation, VARTRANS.Res.CATEGORY, false, graphs)) {

            if (it.hasNext()) {
                ARTNode next = it.next();

                if (next.isURIResource()) {
                    return next.asURIResource();
                }
            }
        }

        return null;

    }

    @Override
    public void setCategory(ARTURIResource lexicoSematicRelation, ARTURIResource category, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(lexicoSematicRelation, VARTRANS.Res.CATEGORY, NodeFilters.ANY, graphs);
        baseRep.addTriple(lexicoSematicRelation, VARTRANS.Res.CATEGORY, category, graphs);

    }

    @Override
    public ARTURIResourceIterator listSources(ARTURIResource target, ARTURIResource category, ARTResource... graphs) throws ModelAccessException {

        Collection<ARTURIResource> result = new ArrayList<ARTURIResource>();

        String queryString = "select distinct ?source where { "
                + "{?lexSemRel " + RDFNodeSerializer.toNT(VARTRANS.Res.SOURCE) + " ?source ."
                + "?lexSemRel " + RDFNodeSerializer.toNT(VARTRANS.Res.CATEGORY) + " ?category ."
                + "?lexSemRel " + RDFNodeSerializer.toNT(VARTRANS.Res.TARGET) + " ?target ."
                + "} union {?source ?category ?target}}";

        try {
            TupleQuery query = baseRep.createTupleQuery(QueryLanguage.SPARQL, queryString, baseRep.getBaseURI());

            query.setBinding("category", category);
            query.setBinding("target", target);

            try (TupleBindingsIterator it = query.evaluate(true)) {

                while (it.hasNext()) {

                    TupleBindings next = it.next();

                    if (next.getBoundValue("source").isURIResource()) {

                        result.add(next.getBoundValue("source").asURIResource());

                    }

                }

            }

        } catch (UnsupportedQueryLanguageException | MalformedQueryException | QueryEvaluationException ex) {

            throw new ModelAccessException(ex);

        }

        return RDFIterators.createARTURIResourceIterator(result.iterator());

    }

    @Override
    public ARTURIResourceIterator listTargets(ARTURIResource source, ARTURIResource category, ARTResource... graphs) throws ModelAccessException {

        Collection<ARTURIResource> result = new ArrayList<ARTURIResource>();

        String queryString = "select distinct ?target where { "
                + "{?lexSemRel " + RDFNodeSerializer.toNT(VARTRANS.Res.SOURCE) + " ?source ."
                + "?lexSemRel " + RDFNodeSerializer.toNT(VARTRANS.Res.CATEGORY) + " ?category ."
                + "?lexSemRel " + RDFNodeSerializer.toNT(VARTRANS.Res.TARGET) + " ?target ."
                + "} union {?source ?category ?target}}";

    
        
        try {
            TupleQuery query = baseRep.createTupleQuery(QueryLanguage.SPARQL, queryString, baseRep.getBaseURI());

            query.setBinding("category", category);
            query.setBinding("source", source);

        
            
            try (TupleBindingsIterator it = query.evaluate(true)) {

                while (it.hasNext()) {

                    TupleBindings next = it.next();

                    if (next.getBoundValue("target").isURIResource()) {

                        result.add(next.getBoundValue("target").asURIResource());

                    }

                }

            }

        } catch (UnsupportedQueryLanguageException | MalformedQueryException | QueryEvaluationException ex) {

            throw new ModelAccessException(ex);

        }

        return RDFIterators.createARTURIResourceIterator(result.iterator());

    }

    @Override
    public ARTURIResourceIterator listSenseRel(ARTResource... graphs) throws ModelAccessException {
      

        Collection<ARTURIResource> result = new ArrayList<ARTURIResource>();

        String queryString = "select distinct ?senRel where { "
                + "{?lexSemRel " + RDFNodeSerializer.toNT(RDF.Res.TYPE) + " " + RDFNodeSerializer.toNT(VARTRANS.Res.TERMINOLOGICAL_RELATION) + " ."
                + "?lexSemRel " + RDFNodeSerializer.toNT(VARTRANS.Res.CATEGORY) + " ?senRel ."
                + "} union {?senRel " + RDFNodeSerializer.toNT(RDFS.Res.SUBPROPERTYOF) + " " + RDFNodeSerializer.toNT(VARTRANS.Res.SENSE_REL) + "}"
                + "filter(?senRel != " + RDFNodeSerializer.toNT(VARTRANS.Res.SENSE_REL) + ")."
                + "filter(?senRel != " + RDFNodeSerializer.toNT(VARTRANS.Res.TRANSLATION_PROPERTY) + ").}";

        try {
            TupleQuery query = baseRep.createTupleQuery(QueryLanguage.SPARQL, queryString, baseRep.getBaseURI());

          
            try (TupleBindingsIterator it = query.evaluate(true)) {

                while (it.hasNext()) {

                    TupleBindings next = it.next();

                    if (next.getBoundValue("senRel").isURIResource()) {

                        result.add(next.getBoundValue("senRel").asURIResource());

                    }

                }

            }

        } catch (UnsupportedQueryLanguageException | MalformedQueryException | QueryEvaluationException ex) {

            throw new ModelAccessException(ex);

        }

        return RDFIterators.createARTURIResourceIterator(result.iterator());
    }

    @Override
    public ARTURIResourceIterator listLexicalRel(ARTResource... graphs) throws ModelAccessException {

        Collection<ARTURIResource> result = new ArrayList<ARTURIResource>();

        String queryString = "select distinct ?lexRel where { "
                + "{?lexSemRel " + RDFNodeSerializer.toNT(RDF.Res.TYPE) + " " + RDFNodeSerializer.toNT(VARTRANS.Res.LEXICAL_RELATION) + " ."
                + "?lexSemRel " + RDFNodeSerializer.toNT(VARTRANS.Res.CATEGORY) + " ?lexRel ."
                + "} union {?lexRel " + RDFNodeSerializer.toNT(RDFS.Res.SUBPROPERTYOF) + " " + RDFNodeSerializer.toNT(VARTRANS.Res.LEXICAL_REL) + "}"
                + "filter(?lexRel != " + RDFNodeSerializer.toNT(VARTRANS.Res.LEXICAL_REL) + ")."
                + "filter(?lexRel != " + RDFNodeSerializer.toNT(VARTRANS.Res.TRANSLATABLE_AS) + ").}";

        try {
            TupleQuery query = baseRep.createTupleQuery(QueryLanguage.SPARQL, queryString, baseRep.getBaseURI());

          
            try (TupleBindingsIterator it = query.evaluate(true)) {

                while (it.hasNext()) {

                    TupleBindings next = it.next();

                    if (next.getBoundValue("lexRel").isURIResource()) {

                        result.add(next.getBoundValue("lexRel").asURIResource());

                    }

                }

            }

        } catch (UnsupportedQueryLanguageException | MalformedQueryException | QueryEvaluationException ex) {

            throw new ModelAccessException(ex);

        }

        return RDFIterators.createARTURIResourceIterator(result.iterator());

    }

    @Override
    public ARTURIResourceIterator listTranslations(ARTURIResource lexicalSense, ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringNodeIterator(baseRep.listValuesOfSubjPredPair(lexicalSense, VARTRANS.Res.TRANSLATION_PROPERTY, true, graphs));

    }

    @Override
    public void addTranslation(ARTURIResource lexicalSense, ARTURIResource translation, ARTResource... graphs) throws ModelUpdateException {

        baseRep.addTriple(lexicalSense, VARTRANS.Res.TRANSLATION_PROPERTY, translation, graphs);

    }

    @Override
    public void removeTranslation(ARTURIResource lexicalSense, ARTURIResource translation, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(lexicalSense, VARTRANS.Res.TRANSLATION_PROPERTY, translation, graphs);

    }

    @Override
    public ARTURIResourceIterator listTranslatableAs(ARTURIResource lexicalEntry, ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringNodeIterator(baseRep.listValuesOfSubjPredPair(lexicalEntry, VARTRANS.Res.TRANSLATABLE_AS, true, graphs));

    }

    @Override
    public void addTranslatableAs(ARTURIResource lexicalEntry, ARTURIResource translation, ARTResource... graphs) throws ModelUpdateException {

        baseRep.addTriple(lexicalEntry, VARTRANS.Res.TRANSLATABLE_AS, translation, graphs);

    }

    @Override
    public void removeTranslatableAs(ARTURIResource lexicalEntry, ARTURIResource translation, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(lexicalEntry, VARTRANS.Res.TRANSLATABLE_AS, translation, graphs);

    }

    @Override
    public ARTURIResourceIterator listTrans(ARTURIResource translationSet, ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringNodeIterator(baseRep.listValuesOfSubjPredPair(translationSet, VARTRANS.Res.TRANS, true, graphs));

    }

    @Override
    public void addTrans(ARTURIResource translationSet, ARTURIResource translation, ARTResource... graphs) throws ModelUpdateException {

        baseRep.addTriple(translationSet, VARTRANS.Res.TRANS, translation, graphs);

    }

    @Override
    public void removeTrans(ARTURIResource translationSet, ARTURIResource translation, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(translationSet, VARTRANS.Res.TRANS, translation, graphs);

    }

}
