/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.vartrans.model;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;

/**
 *
 * @author Andrea
 */
public interface VARTRANSModel {
    
    public boolean isLexicoSemanticRelation(ARTURIResource resource,ARTResource... graphs) throws ModelAccessException;
    public boolean isLexicalRelation(ARTURIResource resource,ARTResource... graphs) throws ModelAccessException;
    public boolean isLexicoSenseRelation(ARTURIResource resource,ARTResource... graphs) throws ModelAccessException;
    public boolean isTerminologicalRelation(ARTURIResource resource,ARTResource... graphs) throws ModelAccessException;
    public boolean isTranslation(ARTURIResource resource,ARTResource... graphs) throws ModelAccessException;
    public boolean isTranslationSet(ARTURIResource resource,ARTResource... graphs) throws ModelAccessException;

    public ARTURIResourceIterator listLexicoSemanticRelations(ARTResource... graphs) throws ModelAccessException;
    public ARTURIResourceIterator listLexicalRelations(ARTResource... graphs) throws ModelAccessException;
    public ARTURIResourceIterator listLexicoSenseRelations(ARTResource... graphs) throws ModelAccessException;
    public ARTURIResourceIterator listTerminologicalRelations(ARTResource... graphs) throws ModelAccessException;
    public ARTURIResourceIterator listTranslations(ARTResource... graphs) throws ModelAccessException;
    public ARTURIResourceIterator listTranslationSets(ARTResource... graphs) throws ModelAccessException;

    public ARTURIResource getSource(ARTURIResource lexicoSematicRelation, ARTResource... graphs) throws ModelAccessException;
    public void setSource(ARTURIResource lexicoSematicRelation,ARTURIResource source, ARTResource... graphs) throws ModelUpdateException;
    public ARTURIResource getTarget(ARTURIResource lexicoSematicRelation, ARTResource... graphs) throws ModelAccessException;
    public void setTarget(ARTURIResource lexicoSematicRelation,ARTURIResource target, ARTResource... graphs) throws ModelUpdateException;
    public ARTURIResource getCategory(ARTURIResource lexicoSematicRelation, ARTResource... graphs) throws ModelAccessException;
    public void setCategory(ARTURIResource lexicoSematicRelation,ARTURIResource category, ARTResource... graphs) throws ModelUpdateException;
    
    public ARTURIResourceIterator listSources(ARTURIResource target,ARTURIResource category,ARTResource... graphs) throws ModelAccessException;
    public ARTURIResourceIterator listTargets(ARTURIResource source,ARTURIResource category,ARTResource... graphs) throws ModelAccessException;

    public ARTURIResourceIterator listSenseRel(ARTResource... graphs) throws ModelAccessException;
    public ARTURIResourceIterator listLexicalRel(ARTResource... graphs) throws ModelAccessException;
    
    public ARTURIResourceIterator listTranslations(ARTURIResource lexicalSense,ARTResource... graphs) throws ModelAccessException;
    public void addTranslation(ARTURIResource lexicalSense,ARTURIResource translation,ARTResource... graphs) throws ModelUpdateException ;
    public void removeTranslation(ARTURIResource lexicalSense,ARTURIResource translation,ARTResource... graphs) throws ModelUpdateException;
    
    public ARTURIResourceIterator listTranslatableAs(ARTURIResource lexicalEntry,ARTResource... graphs) throws ModelAccessException;
    public void addTranslatableAs(ARTURIResource lexicalEntry,ARTURIResource translation,ARTResource... graphs) throws ModelUpdateException ;
    public void removeTranslatableAs(ARTURIResource lexicalEntry,ARTURIResource translation,ARTResource... graphs) throws ModelUpdateException;
    
    public ARTURIResourceIterator listTrans(ARTURIResource translationSet,ARTResource... graphs) throws ModelAccessException;
    public void addTrans(ARTURIResource translationSet,ARTURIResource translation,ARTResource... graphs) throws ModelUpdateException ;
    public void removeTrans(ARTURIResource translationSet,ARTURIResource translation,ARTResource... graphs) throws ModelUpdateException;
   
    
}
